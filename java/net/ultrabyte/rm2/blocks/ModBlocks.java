package net.ultrabyte.rm2.blocks;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.item.ItemStack;
import net.ultrabyte.rm2.main.mainRegistry;



public class ModBlocks {
	public static final String MODID = "redMatter";

	
	public static Block foodORE, deactiveMatter ,redore, white, blockDepletedMatter, pavedSilethialite, blockSilethialite, seed;


	

	
	public void loadBlocks() {
		
		
		
		
	
		seed = new theseed().setBlockName("Seed").setCreativeTab(mainRegistry.core).setHardness(4.0f).setBlockTextureName(MODID + ":" + "blockSeed");
		GameRegistry.registerBlock(seed, "seed");
		
		deactiveMatter = new deactiveMatter().setBlockName("deactive").setCreativeTab(mainRegistry.world).setHardness(1.0f).setBlockTextureName(MODID + ":" + "deactiveMatter");
		GameRegistry.registerBlock(deactiveMatter, "deactivematter");
		
		blockDepletedMatter = new blockDepletedMatter().setBlockName("dstone").setLightLevel(0.1f).setCreativeTab(mainRegistry.world).setHardness(1.0f).setBlockTextureName(MODID + ":" + "depletedMatter");
		GameRegistry.registerBlock(blockDepletedMatter, "blockDepletedMatter");
		
		
		blockSilethialite = new blockSilethialite().setBlockName("Silethialite").setCreativeTab(mainRegistry.world).setHardness(2.0f).setResistance(6000).setBlockTextureName(MODID + ":" + "BlockSilethialite");
    	GameRegistry.registerBlock(blockSilethialite, "blockSilethialite");
		
		
    	pavedSilethialite = new pavedSilethialite().setBlockName("PavedSilethialite").setCreativeTab(mainRegistry.world).setHardness(2.0f).setResistance(6000).setBlockTextureName(MODID + ":" + "BlockPavedSilethialite");
    	GameRegistry.registerBlock(pavedSilethialite, "pavedSilethialite");
    	
    	
		redore = new redore().setBlockName("Red matter Crystal ore").setCreativeTab(mainRegistry.world).setHardness(2.0f).setLightLevel(1f).setBlockTextureName(MODID + ":" + "blockore1");
		GameRegistry.registerBlock(redore, "redore");
		
		white = new white().setBlockName("Tile").setCreativeTab(mainRegistry.core).setHardness(3.0f).setBlockTextureName(MODID + ":" + "blockTile").setResistance(6000);
		GameRegistry.registerBlock(white, "white");
	
    
    	
    	
	}
}
    	
    	
		


