package net.ultrabyte.rm2.blocks;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.ultrabyte.rm2.items.ModItems;

public class foodORE extends Block {

	protected foodORE() {
		super(Material.rock);
		// Lets hope iChun does'nt see this!
	}
	@Override
    public Item getItemDropped(int metadata, Random random, int fortune) {
        return Items.cooked_porkchop;
	}
	public int quantityDropped(Random rand) {
		return rand.nextInt(5 - 1) + 1;
   }
}