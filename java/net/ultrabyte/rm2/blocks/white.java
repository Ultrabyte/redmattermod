package net.ultrabyte.rm2.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

public class white extends Block {

	protected white() {
		super(Material.iron);

		setLightLevel(1f);
	}

}
