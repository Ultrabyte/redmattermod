package net.ultrabyte.rm2.blocks;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.world.World;
import net.ultrabyte.rm2.items.ModItems;

public class deactiveMatter extends Block {

	protected deactiveMatter() {
		super(Material.rock);
		this.setTickRandomly(true);

	}
	@Override
    public Item getItemDropped(int metadata, Random random, int fortune) {
        return ModItems.essence;
	}
	public int quantityDropped(Random rand) {
		return rand.nextInt(2 - 1) + 1;
   }

    public void updateTick(World var6, int var7, int var8, int var9, Random var10)
    {
        if (!var6.isRemote)
        {
            int hold = 0;
			if (hold == 1)
            {
                var6.setBlock(var7, var8, var9, ModBlocks.deactiveMatter);
            }
            else if (var6.getBlockLightValue(var7, var8 + 1, var9) >= 9)
            {
                for (int l = 0; l < 45; ++l)
                {
                    int i1 = var7 + var10.nextInt(3) - 1;
                    int j1 = var8 + var10.nextInt(5) - 3;
                    int k1 = var9 + var10.nextInt(3) - 1;
                    Block block = var6.getBlock(i1, j1 + 1, k1);

                    if (var6.getBlock(i1, j1, k1) == ModBlocks.blockDepletedMatter && var6.getBlockMetadata(i1, j1, k1) == 0 && var6.getBlockLightValue(i1, j1 + 1, k1) >= 4 && var6.getBlockLightOpacity(i1, j1 + 1, k1) <= 2)
                    {
                    	var6.setBlock(i1, j1, k1, ModBlocks.deactiveMatter);                
                    	}
                    } 
                }
            }
        }
}
