package net.ultrabyte.rm2.blocks;

import java.util.Random;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.DamageSource;
import net.minecraft.world.World;
import net.ultrabyte.rm2.items.ModItems;


public class blockDepletedMatter extends Block {

	protected blockDepletedMatter() {
		super(Material.iron);
		this.setTickRandomly(true);
		

		
	}
	@Override
    public Item getItemDropped(int metadata, Random random, int fortune) {
        return ModItems.essence;
	}
	public int quantityDropped(Random rand) {
		return rand.nextInt(8 - 1) + 1;
   }
    public void updateTick(World var6, int var7, int var8, int var9, Random var10)
    {
        if (!var6.isRemote)
        {
            if (var6.getBlockLightValue(var7, var8 + 1, var9) > 1 && var6.getBlockLightOpacity(var7, var8 + 1, var9) > 1)
            {
                var6.setBlock(var7, var8, var9, ModBlocks.blockDepletedMatter);
            }
            else if (var6.getBlockLightValue(var7, var8 + 1, var9) >= 9)
            {
                for (int l = 0; l < 45; ++l)
                {
                    int i1 = var7 + var10.nextInt(3) - 1;
                    int j1 = var8 + var10.nextInt(5) - 3;
                    int k1 = var9 + var10.nextInt(3) - 1;
                    
                    //the list of death and lazyness
                    if (var6.getBlock(i1, j1, k1) != Blocks.air  && var6.getBlock(i1, j1, k1) != ModBlocks.blockDepletedMatter && var6.getBlock(i1, j1, k1) != ModBlocks.deactiveMatter
                    		&& var6.getBlock(i1, j1, k1) != Blocks.bedrock && var6.getBlock(i1, j1, k1) != Blocks.iron_ore && var6.getBlock(i1, j1, k1) != Blocks.stone
                    		&& var6.getBlock(i1, j1, k1) != Blocks.gold_ore && var6.getBlock(i1, j1, k1) != Blocks.coal_ore && var6.getBlock(i1, j1, k1) != Blocks.diamond_ore
                    		&& var6.getBlock(i1, j1, k1) != Blocks.dirt && var6.getBlock(i1, j1, k1) != Blocks.water && var6.getBlock(i1, j1, k1) != Blocks.lava &&
                    		var6.getBlock(i1, j1, k1) != Blocks.emerald_ore && var6.getBlock(i1, j1, k1) != ModBlocks.blockSilethialite && var6.getBlock(i1, j1, k1) != ModBlocks.deactiveMatter
                    		&& var6.getBlock(i1, j1, k1) != Blocks.carpet && var6.getBlock(i1, j1, k1) != Blocks.tallgrass  && var6.getBlock(i1, j1, k1) != ModBlocks.pavedSilethialite
                    		)
                        {
                    	if((Math.random()*100) <= 79){
                    	var6.setBlock(i1, j1, k1, ModBlocks.blockDepletedMatter);
                        System.out.println("Creating depleted matter");
                    
                        if((Math.random()*100) <= 10){
                        	var6.setBlock(i1, j1, k1, Blocks.iron_ore);
                        }
                        if((Math.random()*100) <= 15){
                        	var6.setBlock(i1, j1, k1, Blocks.coal_block);
                        }
                        if((Math.random()*100) <= 8){
                        	var6.setBlock(i1, j1, k1, Blocks.gold_ore);
                        
                        }
                        if((Math.random()*100) <= 3){
                        	var6.setBlock(i1, j1, k1, Blocks.emerald_ore);
                        }
                    	if((Math.random()*100) <= 3){
                        	var6.setBlock(i1, j1, k1, ModBlocks.deactiveMatter);            
                        	}
                        if((Math.random()*100) <= 3){
                        	var6.setBlock(i1, j1, k1, Blocks.diamond_ore);
                            
                        }
               
                    } 
                    	
                }
            }
        }}
    }

public void randomDisplayTick(World par1World, int par2, int par3, int par4, Random par5Random)
    {

        for (int l = 0; l < 1; ++l)
        {
        	double d0 = (double)((float)par2 + par5Random.nextFloat());
            double d1 = (double)((float)par3 + par5Random.nextFloat());
            double d2 = (double)((float)par4 + par5Random.nextFloat());
            double d3 = 0.0D;
            double d4 = 0.0D;
            double d5 = 0.0D;
            int i1 = par5Random.nextInt(2) * 2 - 1;
            d3 = ((double)par5Random.nextFloat() - 0.5D) * 0.49999999850988386D;
            d4 = ((double)par5Random.nextFloat() - 0.5D) * 0.49999999850988386D;
            d5 = ((double)par5Random.nextFloat() - 0.5D) * 0.49999999850988386D;
            par1World.spawnParticle("angryVillager", d0, d1, d2, d3, d4, d5);
        }
    }
	
}


