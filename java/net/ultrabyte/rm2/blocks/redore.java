package net.ultrabyte.rm2.blocks;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;
import net.ultrabyte.rm2.items.ModItems;

public class redore extends Block {

	protected redore() {
		super(Material.rock);

	}
	@Override
    public Item getItemDropped(int metadata, Random random, int fortune) {
        return ModItems.redmatterShard;
    }
	public int quantityDropped(Random rand) {
		return rand.nextInt(5 - 1) + 1;
    }
}