package net.ultrabyte.rm2.main;

import java.util.Random;

import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.ultrabyte.rm2.blocks.ModBlocks;
import net.ultrabyte.rm2.items.ModItems;
import net.ultrabyte.rm2.items.redmatterBread;
import net.ultrabyte.rm2.pills.redmatterSnack;
import net.ultrabyte.rm2.pills.speed;
import net.ultrabyte.rm2.world.redmatteroreWorld;
import net.ultrabyte.rm2.world.Silethialite;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.GameRegistry;

public class imports {
	
	ModBlocks mBlock = new ModBlocks();
	ModItems mItem = new ModItems();
	redmatterSnack food = new redmatterSnack();
	speed speed = new speed();
	net.ultrabyte.rm2.pills.jump jump = new net.ultrabyte.rm2.pills.jump();
	net.ultrabyte.rm2.pills.hunger hunger = new net.ultrabyte.rm2.pills.hunger();
	net.ultrabyte.rm2.pills.voldermort voldermort = new net.ultrabyte.rm2.pills.voldermort();
   net.ultrabyte.rm2.pills.Regen regen = new net.ultrabyte.rm2.pills.Regen();
   net.ultrabyte.rm2.pills.pilloftheexplorer explore = new net.ultrabyte.rm2.pills.pilloftheexplorer();
   net.ultrabyte.rm2.pills.jetpill jetpill = new net.ultrabyte.rm2.pills.jetpill();

	recipes rec = new recipes();
    public static final String MODID = "redMatter";
	
	
	
    public void preInit(FMLPreInitializationEvent e) {
    	food.main();
    	speed.main();
    	voldermort.main();
        mBlock.loadBlocks();
    	mItem.loadItems();
    	hunger.main();
    	explore.main();
    	rec.preInit(e);
    	Silethialite.mainRegistry();

    	
    }
	
}
