package net.ultrabyte.rm2.main;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.MinecraftForgeClient;
import net.ultrabyte.rm2.blocks.ModBlocks;
import net.ultrabyte.rm2.items.ModItems;
import net.ultrabyte.rm2.items.redmatterBread;
import net.ultrabyte.rm2.pills.redmatterSnack;
import net.ultrabyte.rm2.pills.speed;
import net.ultrabyte.rm2.proxy.ProxyCommon;
import net.ultrabyte.rm2.world.redmatteroreWorld;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;
import cpw.mods.fml.relauncher.Side;



@Mod(modid = mainRegistry.MODID, version = mainRegistry.VERSION)
public class mainRegistry
{
	

	
	public void var(){
		
	}
	public static Item redmatterBread = new ItemFood(32, 20, false);
	
	public static Block blockBlack;
	
 imports in = new imports();
	
    public static final String MODID = "redMatter";
    public static final String VERSION = "1.0";
    public int rmp;
   
    
    @Instance(value = MODID)
    public static mainRegistry instance;

    
    public static CreativeTabs pill = new CreativeTabs("pills"){
        public Item getTabIconItem(){
        	return Items.sugar;
        }
    };
    
    
    public static CreativeTabs world = new CreativeTabs("redmatterworld"){
        public Item getTabIconItem(){
        	return ModItems.rht2;
        }
    };
    
    public static CreativeTabs core = new CreativeTabs("redmattercore"){
    public Item getTabIconItem(){
    	return ModItems.rmt5;
       }
    };
    
    
    
    @SidedProxy(clientSide = "net.ultrabyte.rm2.proxy.ProxyClient", serverSide = "net.ultrabyte.rm2.proxy.ProxyCommon" )
    public static ProxyCommon proxy;
    @EventHandler
    
    
    public void preInit(FMLPreInitializationEvent e) {
in.preInit(e);
    }
    
    public void init(FMLInitializationEvent event)
    {
    	
    	
    	

    }
    
    
    @EventHandler
    public void load(FMLInitializationEvent event) {
    	proxy.registerRenderers();
    	

    	
    	LanguageRegistry.instance().addStringLocalization("itemGroup.redTab", "en_US", "Red Matter Mod");
    
    	
    }
}
