package net.ultrabyte.rm2.main;

import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.ultrabyte.rm2.blocks.ModBlocks;
import net.ultrabyte.rm2.items.ModItems;
import net.ultrabyte.rm2.items.redmatterBread;
import net.ultrabyte.rm2.pills.redmatterSnack;
import net.ultrabyte.rm2.pills.speed;
import net.ultrabyte.rm2.world.redmatteroreWorld;
import net.ultrabyte.rm2.world.Silethialite;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.GameRegistry;

public class recipes{

 public static final String MODID = "redMatter";
	
	
	
    public void preInit(FMLPreInitializationEvent e) {



	//Shapeless recipes
	
    	GameRegistry.addShapelessRecipe(new ItemStack(Items.carrot,3),Blocks.dirt, Items.apple);
    	GameRegistry.addShapelessRecipe(new ItemStack(Items.potato,3),Blocks.dirt, Items.carrot);
		GameRegistry.addShapelessRecipe(new ItemStack(ModItems.essence,1), ModItems.redmatterShard);
		GameRegistry.addShapelessRecipe(new ItemStack(ModItems.essence,2),ModItems.essence, Blocks.redstone_block);



	//Shaped recipes
		GameRegistry.addShapedRecipe(new ItemStack (ModItems.rmt5, 1),"XXX","XOX","XXX",'O',Blocks.diamond_block,'X',ModItems.rmt4);
		GameRegistry.addShapedRecipe(new ItemStack (ModItems.rmt4, 1),"XXX","XOX","XXX",'O',Blocks.emerald_block,'X',ModItems.rmt3);
		GameRegistry.addShapedRecipe(new ItemStack (ModItems.rmt3, 1),"XXX","XOX","XXX",'O',Blocks.obsidian,'X',ModItems.rmt2);
		GameRegistry.addShapedRecipe(new ItemStack (ModItems.rmt2, 1),"XXX","XOX","XXX",'O',Blocks.gold_block,'X',ModItems.rmt1);
		GameRegistry.addShapedRecipe(new ItemStack (ModItems.rmt1, 1),"XXX","XOX","XXX",'O',Blocks.iron_block,'X',ModItems.essence);
    	GameRegistry.addShapedRecipe(new ItemStack (redmatterSnack.snack, 2),"XOX","XPX","XOX",'O',Items.bread,'P',ModItems.redmatterShard);
    	GameRegistry.addShapedRecipe(new ItemStack (ModItems.rht1, 1),"QOQ","QXQ","QXQ",'O',ModItems.rmt2,'X',Items.diamond);
    	GameRegistry.addShapedRecipe(new ItemStack (ModItems.rht2, 1),"QOQ","QXQ","QXQ",'O',ModItems.rmt5,'X',ModItems.rht1);
    	GameRegistry.addShapedRecipe(new ItemStack (ModItems.rpt1, 1),"OOO","QXQ","QXQ",'O',ModItems.rmt2,'X',Items.diamond);
    	GameRegistry.addShapedRecipe(new ItemStack (ModItems.rpt2, 1),"OOO","QXQ","QXQ",'O',ModItems.rmt5,'X',ModItems.rpt1);
    	GameRegistry.addShapedRecipe(new ItemStack (ModItems.rst1, 1),"QOQ","QOQ","QXQ",'O',ModItems.rmt2,'X',Items.diamond);
    	GameRegistry.addShapedRecipe(new ItemStack (ModItems.rst2, 1),"QOQ","QOQ","QXQ",'O',ModItems.rmt5,'X',ModItems.rst1);
    	GameRegistry.addShapedRecipe(new ItemStack (ModItems.rat1, 1),"OOQ","OXQ","QXQ",'O',ModItems.rmt2,'X',Items.diamond);
    	GameRegistry.addShapedRecipe(new ItemStack (ModItems.rat2, 1),"OOQ","OXQ","QXQ",'O',ModItems.rmt5,'X',ModItems.rat1);
    	GameRegistry.addShapedRecipe(new ItemStack (ModBlocks.seed, 1),"OOO","OXO","OOO",'O',Items.iron_ingot,'X',ModItems.essence);
	
	
	
	//Smelting recipes
	
	
    	GameRegistry.addSmelting(Blocks.gravel,new ItemStack(ModBlocks.blockSilethialite), 500.0f);
    	GameRegistry.addSmelting(ModBlocks.blockSilethialite,new ItemStack(ModBlocks.pavedSilethialite), 500.0f);
    	GameRegistry.addSmelting(Blocks.stone,new ItemStack(Blocks.gravel), 500.0f);
	
	

	}

}