package net.ultrabyte.rm2.world;

import cpw.mods.fml.common.IWorldGenerator;
import cpw.mods.fml.common.registry.GameRegistry;

public class redmatteroreWorld {
	
	public static void mainRegistry(){
		initWorldGen();
	}

	public static void initWorldGen(){
		registerWorldGen(new WorldGenOre() ,1);
	}
	
	public static void registerWorldGen(IWorldGenerator worldGenClass, int weightedProbability) {
		GameRegistry.registerWorldGenerator(worldGenClass, weightedProbability);
	}
	
}
