package net.ultrabyte.rm2.items;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Item.ToolMaterial;
import net.minecraftforge.common.util.EnumHelper;
import net.ultrabyte.rm2.main.mainRegistry;



public class ModItems {
	public static String MODID = "redMatter";
	
	public static Item atom, essence, redmatterShard, infusedDiamond,redmatter, rawMatter, rmt1, rmt2, rmt3, rmt4, rmt5, QI, CI, QP, CP, UniMatter, RTP, ARTP ;

	public static Item rpt1;
	public static Item rpt2;
	public static Item rst1;
	public static Item rst2;
	public static Item rat1;
	public static Item rat2;
	public static Item rht1;
	public static Item rht2;
	
	
	public static ToolMaterial t1 = EnumHelper.addToolMaterial("rmtm", 5, -1, 9.0F, 6, 0);
	public static ToolMaterial w1 = EnumHelper.addToolMaterial("rmtm", 5, -1, 5.0F, 16, 0);
	public static ToolMaterial t2 = EnumHelper.addToolMaterial("rmtm", 5, -1, 21.0F, 10, 0);
	public static ToolMaterial w2 = EnumHelper.addToolMaterial("rmtm", 5, -1, 8.0F, 28, 0);
	public static ToolMaterial sh = EnumHelper.addToolMaterial("rmtm", 5, -1, 8.0F, 6, 0);
	
	
	net.ultrabyte.rm2.items.lava lava;


	potatoSword potato;

	ItemFly fly;
	
	harvester HarvesterT2;





	public void loadItems() {
		
		
		UniMatter = new Item().setUnlocalizedName("UniMatter").setTextureName(MODID + ":" + "UniversalRedMatter").setCreativeTab(mainRegistry.core);
		GameRegistry.registerItem(UniMatter, "UniMatter");
		
		ARTP = new Item().setUnlocalizedName("ARTP").setFull3D().setCreativeTab(mainRegistry.core).setTextureName(MODID + ":" + "AdvancedRedTechProcesser");
		GameRegistry.registerItem(ARTP, "ARTP");
		
		RTP = new Item().setUnlocalizedName("RTP").setFull3D().setCreativeTab(mainRegistry.core).setTextureName(MODID + ":" + "RedTechProcesser");
		GameRegistry.registerItem(RTP, "RTP");
		
		CP = new Item().setUnlocalizedName("CP").setFull3D().setCreativeTab(mainRegistry.core).setTextureName(MODID + ":" + "carbonPlate");
		GameRegistry.registerItem(CP, "CP");
		
		CI = new Item().setUnlocalizedName("CI").setFull3D().setCreativeTab(mainRegistry.core).setTextureName(MODID + ":" + "carbonIngot");
		GameRegistry.registerItem(CI, "CI");
		
		QP = new Item().setUnlocalizedName("QP").setFull3D().setCreativeTab(mainRegistry.core).setTextureName(MODID + ":" + "QuantumIronPlate");
		GameRegistry.registerItem(QP,"QP");
		
		QI = new Item().setUnlocalizedName("QI").setFull3D().setCreativeTab(mainRegistry.core).setTextureName(MODID + ":" + "QuantumIron");
		GameRegistry.registerItem(QI, "QI");
		
		rpt1 = new rpt1(t1).setUnlocalizedName("redpick1").setCreativeTab(mainRegistry.core).setTextureName(MODID + ":" + "rpt1");
		GameRegistry.registerItem(rpt1, "rpt1");
		
		rst1 = new rst1(w1).setUnlocalizedName("redsword1").setFull3D().setCreativeTab(mainRegistry.core).setTextureName(MODID + ":" + "rst1");
		GameRegistry.registerItem(rst1, "rst1");
		
		rat1 = new rat1(t1).setUnlocalizedName("redaxe1").setCreativeTab(mainRegistry.core).setTextureName(MODID + ":" + "rat1");
		GameRegistry.registerItem(rat1, "rat1");
		
		rht1 = new rht1(t1).setUnlocalizedName("redspade1").setCreativeTab(mainRegistry.core).setTextureName(MODID + ":" + "rht1");
		GameRegistry.registerItem(rht1, "rht1");
		
		rpt2 = new rpt2(t2).setUnlocalizedName("redpick2").setCreativeTab(mainRegistry.core).setTextureName(MODID + ":" + "rpt2");
		GameRegistry.registerItem(rpt2, "rpt2");
		
		rst2 = new rst2(w2).setUnlocalizedName("redsword2").setCreativeTab(mainRegistry.core).setTextureName(MODID + ":" + "rst2");
		GameRegistry.registerItem(rst2, "rst2");
		
		rat2 = new rat2(t2).setUnlocalizedName("redaxe2").setCreativeTab(mainRegistry.core).setTextureName(MODID + ":" + "rat2");
		GameRegistry.registerItem(rat2, "rat2");
		
		rht2 = new rht2(sh).setUnlocalizedName("redspade2").setCreativeTab(mainRegistry.core).setTextureName(MODID + ":" + "rht2");
		GameRegistry.registerItem(rht2, "rht2");
		
		
		
		
		redmatterShard = new Item().setUnlocalizedName("Red matter infused shard").setCreativeTab(mainRegistry.core).setTextureName(MODID + ":" + "itemInfusedShard");
		GameRegistry.registerItem(redmatterShard, "redmatterShard");
		
		essence = new Item().setUnlocalizedName("redmatterEssence").setCreativeTab(mainRegistry.core).setTextureName(MODID + ":" + "redmatterEssence");
		GameRegistry.registerItem(essence, "redmatterEssence");
		
		rmt1 = new Item().setUnlocalizedName("rmt1").setCreativeTab(mainRegistry.core).setTextureName(MODID + ":" + "RedMatterT1");
		GameRegistry.registerItem(rmt1, "rmt1");
		
		rmt2 = new Item().setUnlocalizedName("rmt2").setCreativeTab(mainRegistry.core).setTextureName(MODID + ":" + "RedMatterT2");
		GameRegistry.registerItem(rmt2, "rmt2");
		
		rmt3 = new Item().setUnlocalizedName("rmt3").setCreativeTab(mainRegistry.core).setTextureName(MODID + ":" + "RedMatterT3");
		GameRegistry.registerItem(rmt3, "rmt3");
		
		rmt4 = new Item().setUnlocalizedName("rmt4").setCreativeTab(mainRegistry.core).setTextureName(MODID + ":" + "RedMatterT4");
		GameRegistry.registerItem(rmt4, "rmt4");
		
		rmt5 = new Item().setUnlocalizedName("rmt5").setCreativeTab(mainRegistry.core).setTextureName(MODID + ":" + "RedMatterT5");
		GameRegistry.registerItem(rmt5, "rmt5");
		
		rawMatter = new Item().setUnlocalizedName("rawMatter").setCreativeTab(mainRegistry.core).setTextureName(MODID + ":" + "RawMatter");
		GameRegistry.registerItem(rawMatter, "rawMatter");
		
		lava = new lava();
		
		fly = new ItemFly();
		
		HarvesterT2 = new harvester();
		
		potato = new potatoSword();
		
	}
	
	
}
