package net.ultrabyte.rm2.items;

import java.util.Random;

import cpw.mods.fml.common.event.FMLServerStartingEvent;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.effect.EntityLightningBolt;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;
import net.ultrabyte.rm2.main.mainRegistry;




@SuppressWarnings("unchecked")
public class ItemFly{

	 public static final String MODID = "redMatter";
	
public ItemFly(){}

public static Item block;
public static Object instance;
public void load(){
}



static{
block = (new Itemclick(433));
Item.itemRegistry.addObject(433, "Click", block);

}

static class Itemclick extends Item{

public Itemclick(int par1){
setMaxDamage(500);
maxStackSize = 64;
setUnlocalizedName("fly");
setTextureName(MODID + ":" + "flamingFeather");
setCreativeTab(mainRegistry.core);
GameRegistry.addShapedRecipe(new ItemStack (block, 1),"XXX","XRX","XXX",'X',ModItems.rmt4,'R',Items.feather);
}

public int getMaxItemUseDuration(ItemStack par1ItemStack)
{
    return 500;
}
public float getStrVsBlock(ItemStack par1ItemStack, Block par2Block)
{
    return 1.0F;
}
public ItemStack onItemRightClick(ItemStack itemstack, World world, EntityPlayer entity){
if(entity.inventory.hasItemStack(new ItemStack(ModItems.essence))){
entity.addPotionEffect(new PotionEffect(11, 80, 200));
entity.motionY+= 0.75;

if((Math.random()*100) <= 3){
if(entity instanceof EntityPlayer)((EntityPlayer)entity).inventory.consumeInventoryItem(ModItems.essence);
}
} else {
	itemstack.damageItem(1, entity);
	entity.addPotionEffect(new PotionEffect(11, 80, 200));
	entity.motionY+= 0.75;
}


return itemstack;
}




}}