package net.ultrabyte.rm2.items;

import java.awt.List;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.ultrabyte.rm2.main.mainRegistry;


public class redmatterBread extends ItemFood{

	public redmatterBread(int i,int j, boolean b) {
		super(j, b);
		
this.setCreativeTab(mainRegistry.core);
this.setTextureName("redMatter:redmatterBread");

	}
	public void addInformation(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, List list, boolean par4)
	{
	list.add("In the name of x3n0ph0b3x!");
	}
}
